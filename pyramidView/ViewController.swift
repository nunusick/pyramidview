//
//  ViewController.swift
//  pyramidView
//
//  Created by Admin on 11/22/18.
//  Copyright © 2018 s. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        squaresCount = 5
        drawLine(count: squaresCount)
        currentDown += specSpacing
        drawStraightAngle()
        currentDown += specSpacing
        drawTriangle()
    }
    
    var squaresCount: Int = 0
    let spacing = 20
    let specSpacing = 20
    var currentDown = 0
    
    static let safeAreaHeight = 50
    static let safeAreaWidth = 20
    
    var squareHeight: Int {
        get {
            return (Int(view.frame.height) - ViewController.safeAreaHeight * 2 - specSpacing * 2) / (squaresCount * 2 + 1)
        }
    }
    
    var squareWidth: Int {
        get {
            return (Int(view.frame.width) - ViewController.safeAreaWidth * 2) / squaresCount
        }
    }
    
    func makeSquare(x: Int, y: Int, w: Int, h: Int) -> UIView {
        let rect = CGRect.init(x: x, y: y, width: w, height: h)
        let square = UIView.init(frame: rect)
        
        print(square.frame)
        square.backgroundColor = UIColor.brown
        view.backgroundColor = UIColor.yellow
        return square
    }
    
    func drawLine(count: Int) {
        
        for i in 0..<count {
            let vw = makeSquare(x: ViewController.safeAreaWidth +
                                   (Int(view.frame.width) - squaresCount * squareWidth) / 2 +
                                   i * squareWidth,
                                y: ViewController.safeAreaHeight + currentDown,
                                w: squareWidth - spacing,
                                h: squareHeight - spacing)
            view.addSubview(vw)
        }
        
        currentDown += squareHeight
    }
    
    func drawStraightAngle() {
    
        for i in 1...squaresCount {
            drawLine(count: i)
        }
    }
    
    func drawTriangle() {
        
        for i in 1...squaresCount {
            for j in 0..<i {
                let vw = makeSquare(x: ViewController.safeAreaWidth +
                                       (Int(view.frame.width) - i * squareWidth) / 2 +
                                       j * squareWidth,
                                    y: ViewController.safeAreaHeight + currentDown,
                                    w: squareWidth - spacing,
                                    h: squareHeight - spacing)
                view.addSubview(vw)
            }
            
            currentDown += squareHeight
        }
    }
    
    
}

